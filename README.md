# Wentronic PHP API client

This is a client for php 8 and higher.

Install:

```
composer require wentronic/api
```

Using the api:

```php
//Your login email at wentronic.com
$username = 'email@domain.com'; 
//Your login password at wentronic.com
$password = 'password'; 
$client = new Wentronic\Api\WentronicApi($username, $password);
//Get list of locale options
$locales = $client->getLocales();
//Set locale to 'en_EN' for all requests
$client->setLocale('en_EN');
//Get product content with default locale
$data = $client->getProductContent('11525');
//Get product content with specific locale
$data = $client->getProductContent('11525', 'en_EN');
//Get 1 baseline
$data = $client->getProductBaseLine('11525');
//Get multiple baseline
$data = $client->getProductBaseLine(['11525', '11526']);
//For all other calls, providing string with 1 sku or array with sku's works
$data = $client->getProductMedia(['11525', '11526']);
$data = $client->getProductPrice(['11525', '11526']);
$data = $client->getProductStock(['11525', '11526']);
```

