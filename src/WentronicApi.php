<?php

namespace Wentronic\Api;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class WentronicApi {

    protected Client $client;
    protected array $token;
    public int $retries = 10;
    public int $sleep = 10;
    protected string $locale = 'nl_NL';
    public array $locales = [
        'de_DE',
        'es_ES',
        'nl_NL',
        'da_DA',
        'sv_SE',
        'cs_CS',
        'en_EN',
        'it_IT',
        'pl_PL',
        'fr_FR',
    ];

    /**
     * @param $username string Username is usually the email you use to log in on www.wentronic.com
     * @param $password string Password is usually the password you use to log in on www.wentronic.com
     */
    public function __construct(protected string $username, protected string $password) {
        $this->client = new Client([
            'base_uri' => 'https://api.wentronic.com/',
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ]);
        $this->authenticate($username, $password);
    }

    /**
     * Authenticate the client and update the local token
     * @param $username
     * @param $password
     * @return bool True if success, false if failed
     */
    public function authenticate($username, $password): bool {
        try {
            $response = $this
                ->client
                ->post('access-tokens', [
                    RequestOptions::JSON => [
                        'data' => [
                            'type' => 'access-tokens',
                            'attributes' => [
                                'username' => $username,
                                'password' => $password,
                            ]
                        ]
                    ]
                ]);
            $this->token = $this->formatOutput($response);
            return true;
        }
        catch(\Throwable $tr) {
            return false;
        }
    }

    /**
     * Set locale based on list of locales.
     * @param $locale
     * @return void
     * @throws \Exception Thows exception if locale does not exist.
     */
    public function setLocale($locale) {
        if(in_array($locale, $this->locales)) {
            $this->locale = $locale;
        }
        else {
            throw new \Exception("Locale $locale does not exist");
        }
    }

    /**
     * Get all available locales
     * @return array|string[]
     */
    public function getLocales() {
        return $this->locales;
    }

    /**
     * Perform request and retry if failing and re-authenticate if token expired
     * @param $method
     * @param $url
     * @param $params
     * @return mixed
     * @throws \Exception Throws exception if unable to reach api withing $this->retries times
     */
    public function request($method, $url, $params = []) : array {
        $method = strtolower($method);
        $retries = $this->retries;
        while(true) {
            try {
                $type = $method == 'get' ? RequestOptions::QUERY : RequestOptions::JSON;
                $response = $this->client->{$method}($url, [
                    $type => $params,
                    'headers' => [
                        "Authorization" => "Bearer {$this->token['data']['attributes']['accessToken']}",
                    ],
                ]);
                return $this->formatOutput($response);
            }
            catch(\Throwable $tr) {
                if($tr->getCode() == 403) {
                    $this->authenticate($this->username, $this->password);
                }
                else {
                    sleep($this->sleep);
                    $retries--;
                    if(!$retries) {
                        throw new \Exception($tr->getMessage());
                    }
                }
            }
        }
    }

    /**
     * Format the ResponseInterface into an associated array
     * @param ResponseInterface $input
     * @return array
     */
    public function formatOutput(ResponseInterface $input): array {
        return json_decode($input->getBody()->getContents(), true);
    }

    /**
     * Get content of a sku
     * @param $sku
     * @param $locale
     * @return array
     * @throws \Exception
     */
    public function getProductContent($sku, $locale = null) : array  {
        if(empty($locale)) {
            $locale = $this->locale;
        }
        return $this->request('get', "products-data-content/$sku", [
            'locale' => $locale
        ]);
    }

    /**
     * Get baseline for one or more products
     * @param string|array $sku
     * @return array
     * @throws \Exception Throws exception if unable to reach api withing $this->retries times
     */
    public function getProductBaseLine(string|array $sku) : array {
        $params = is_array($sku) ? $sku : [];
        $singleSku = is_array($sku) ? '' : "/$sku";
        return $this->request('get', "products-data-base-line$singleSku", [
            'skus' => $params
        ]);
    }

    /**
     * Get media for one or more products
     * @param string|array $sku
     * @return array
     * @throws \Exception Throws exception if unable to reach api withing $this->retries times
     */
    public function getProductMedia(string|array $sku) : array {
        $params = is_array($sku) ? $sku : [];
        $singleSku = is_array($sku) ? '' : "/$sku";
        return $this->request('get', "products-data-media$singleSku", [
            'skus' => $params
        ]);
    }

    /**
     * Get price for one or more products
     * @param string|array $sku
     * @return array
     * @throws \Exception Throws exception if unable to reach api withing $this->retries times
     */
    public function getProductPrice(string|array $sku) : array {
        $params = is_array($sku) ? $sku : [];
        $singleSku = is_array($sku) ? '' : "/$sku";
        return $this->request('get', "products-data-price$singleSku", [
            'skus' => $params
        ]);
    }

    /**
     * Get stock for one or more products
     * @param string|array $sku
     * @return array
     * @throws \Exception Throws exception if unable to reach api withing $this->retries times
     */
    public function getProductStock(string|array $sku) : array {
        $params = is_array($sku) ? $sku : [];
        $singleSku = is_array($sku) ? '' : "/$sku";
        return $this->request('get', "products-data-stock$singleSku", [
            'skus' => $params
        ]);
    }

}
